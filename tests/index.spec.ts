//********************************************************************************
// Part 2 : Tests
//
// * Describe your process to tests some of the functions and properties above
// * It can either be code, or just commented explanations on the testing procedure
//   (what, how, ..)
//********************************************************************************

import Wallet from "../src/wallet";
import Coin from "../src/coin";
import { it } from "mocha";
import assert = require("assert");

describe('#Wallet.distribution()', function() {
    it('correctly', function () {
        let wallet = new Wallet();
        const coins = [
            new Coin(1_123),
            new Coin(5),
            new Coin(67),
            new Coin(1_000_001),
        ]
        coins.map(coin => wallet.add(coin))

        const distribution = wallet.distribution(1000)
        assert(distribution.buckets[0]?.length === 2)
        assert(distribution.buckets[1]?.length === 1)
        assert(distribution.buckets[2]?.length === 1)
    })
})

describe('#Wallet.available()', function() {
    it('correctly', function () {
        let wallet = new Wallet();
        const coins = [
            new Coin(4),
            new Coin(10),
            new Coin(2),
        ]
        coins.map(coin => wallet.add(coin))
        assert(wallet.available() === 16)
    })

    it('correctly with reserved (reserved coin will be remind in wallet', function () {
        let wallet = new Wallet();
        const reserved = new Coin(4)
        reserved.setReservationId("id")
        const coins = [
            reserved,
            new Coin(10),
            new Coin(2),
        ]
        coins.map(coin => wallet.add(coin))
        assert(wallet.available() === 16)
    })
})

describe('#Wallet.spend()', function() {
    it('spend coin 4', function () {
        let wallet = new Wallet();
        const reserved = new Coin(2)
        reserved.setReservationId("id")
        const coins = [
            reserved,
            new Coin(10),
            new Coin(2),
            new Coin(4),
        ]
        coins.map(coin => wallet.add(coin))
        assert.equal(wallet.spend(4).length, 3)
        assert.equal(wallet.available(), 14)
    })

    it('spend coin 2 & 2', function () {
        let wallet = new Wallet();
        const reserved = new Coin(4)
        reserved.setReservationId("id")
        const coins = [
            reserved,
            new Coin(10),
            new Coin(2),
            new Coin(2),
        ]
        coins.map(coin => wallet.add(coin))
        assert(wallet.spend(4).length === 2)
    })
})

describe('reservation related', function() {
    it('reservation spend', function () {
        let wallet = new Wallet();
        const coins = [
            new Coin(2),
            new Coin(10),
            new Coin(2),
            new Coin(4),
        ]
        coins.map(coin => wallet.add(coin))
        assert(wallet.available() == 18)
        const reservationId = wallet.reserve(6)
        const reservationSpend = wallet.reservationSpend(reservationId)
        assert.equal(reservationSpend.length, 2)
        assert.equal(wallet.available(), 14)
    })
})
