import Coin from './coin';
import Distribution from './distribution';
import Util from './util';

// The Wallet Class
class Wallet {
  private coins: Array<Coin>;

  constructor() {
    // You are free to change the constructor to match your needs.
    this.coins = [];
  }

  //* *******************************************************************************
  // Part 1 : API
  //* *******************************************************************************

  //* *******************************************************************************
  // Part 1.1: return the total amount of coins available in this wallet
  //* *******************************************************************************
  public available(): number {
    return this.coins.map((coin) => coin.getValue()).reduce((a, b) => a + b);
  }

  //* *******************************************************************************
  // Part 1.2: Add coins to this wallet
  //
  // We want the ability to reserve
  //* *******************************************************************************
  public add(coin: Coin) {
    this.coins.push(coin);
  }

  //* *******************************************************************************
  // Part 1.3: Distribution of coins
  //
  // We want to be able to categorize the coins scale distribution we have in the wallet.
  //
  // For example, using a scale of 1000, we want to categorize in bucket of the following range:
  //
  // * bucket[0] : 0 .. 999
  // * bucket[1] : 1_000 .. 999_999
  // * bucket[2] : 1_000_000 .. 999_999_999.
  // * bucket[3] : etc
  //
  // Given the following wallet coins: [1_234, 5, 67, 1_000_001] should result in the following:
  // * bucket[0] : [5, 67]
  // * bucket[1] : [1_234]
  // * bucket[2] : [1_000_001]
  //* *******************************************************************************
  public distribution(scale: number): Distribution {
    const buckets: Coin[][] = [];
    this.coins.forEach((coin) => {
      let exponent = 1;
      while (true) {
        const divisor = Math.floor(coin.getValue() / scale ** exponent);
        if (divisor < 1) {
          if (buckets[exponent - 1]) {
            buckets[exponent - 1].push(coin);
          } else {
            buckets[exponent - 1] = [coin];
          }
          break;
        } else {
          exponent += 1;
        }
      }
    });
    return new Distribution(buckets);
  }

  //* *******************************************************************************
  // Part 1.4: Spending from this wallet a specific amount
  //
  // Try to construct a valid result where the sum of coins return are above the requested
  // amount, and try to stay close to the amount as possible. Explain your choice of
  // algorithm.
  //
  // If the requested cannot be satisfied then an error should be return.
  //* *******************************************************************************
  public spend(amount: number): Array<Coin> {
    const nonReservedCoin = this.coins.filter((coin) => !coin.isReserved());
    const coinsToBeSpent = Util.subsetSum(nonReservedCoin, amount);
    if (coinsToBeSpent.length === 0) throw new Error('Spend Error: Insufficient funds in the wallet.');

    this.coins = this.coins
      .filter((i) => !coinsToBeSpent.find((y) => y.getValue() === i.getValue() && !i.isReserved()));
    return this.coins;
  }

  //* *******************************************************************************
  // Part 1.5: Reserving assets
  //
  // In certain cases, it's important to consider that some coins need to be reserved;
  // for example we want to put aside some coins from a wallet while
  // we conduct other verification, so that once we really want to spend, we
  //
  // We need a way to reserve and keep a handle of this reservation; this works very similarly
  // to the previous part (1.4) except that the fund are kept in the wallet and reserved
  // until the user either 'cancel' or 'spend' this reservation.
  //
  // With cancel, the locked coins are returned to the available funds
  // With spend, the locked coins are remove from the wallet and given to the user
  //* *******************************************************************************
  public reserve(amount: number): string {
    const nonReservedCoin = this.coins.filter((coin) => !coin.isReserved());
    const coinsToBeReserved = Util.subsetSum(nonReservedCoin, amount);
    if (coinsToBeReserved.length === 0) throw new Error('Spend Error: Insufficient funds in the wallet.');

    const rid = `rid-${(new Date()).getTime()}`;
    this.coins
      .filter((i) => coinsToBeReserved.find((y) => y.getValue() === i.getValue() && !i.isReserved()))
      .map((i) => i.setReservationId(rid));
    return rid;
  }

  public reservationSpend(reservationId: string): Array<Coin> {
    const reservationSpend = this.coins.filter((i) => i.getReservationId() === reservationId);
    this.coins = this.coins.filter((i) => i.getReservationId() === reservationId);
    return reservationSpend;
  }

  public reservationCancel(reservationId: string) {
    this.coins
      .filter((i) => i.getReservationId() === reservationId)
      .map((i) => i.setReservationId(''));
  }
}

export default Wallet;
