import Coin from './coin';

class Util {
  static quickSort(coins: Coin[]): Coin[] {
    if (coins.length <= 1) {
      return coins;
    }

    const pivotIndex = 0;
    const pivot = coins[pivotIndex];

    const less: Coin[] = [];
    const greater: Coin[] = [];

    coins.forEach((coin, i) => {
      if (i !== pivotIndex) {
        if (coin.getValue() > pivot.getValue()) {
          greater.push(coin);
        } else {
          less.push(coin);
        }
      }
    });

    return [
      ...this.quickSort(less),
      pivot,
      ...this.quickSort(greater),
    ];
  }

  static subsetSum(coins: Coin[], amount: number): Coin[] {
    const result : Coin[][] = [];

    function worker(list: Coin[], target: number, subset:Coin[] = []) {
      const tempSum = subset.map((coin) => coin.getValue()).reduce((a, b) => a + b, 0);

      if (tempSum === target) result.push(subset);
      if (tempSum >= target) return;

      list.forEach((coin, idx) => {
        const remaining = coins.slice(idx + 1);
        worker(remaining, target, subset.concat(coin));
      });
    }

    worker(coins, amount);
    return result[0];
  }
}

export default Util;
