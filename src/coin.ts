// Represent a specific value of a coin
class Coin {
  private readonly value: number;

  private reservationId: string = ''

  constructor(value: number) {
    this.value = value;
  }

  public isReserved() {
    return !!this.reservationId;
  }

  public getReservationId(): string {
    return this.reservationId;
  }

  public setReservationId(reservationId: string) {
    this.reservationId = reservationId;
  }

  public getValue(): number {
    return this.value;
  }
}

export default Coin;
